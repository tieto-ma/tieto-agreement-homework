AppContainerUi = Ext.extend(Ext.Viewport, {
    layout : {
        align: 'stretch',
        pack: 'start',
        type: 'vbox'
    },

    initComponent : function() {
        this.items = [
            {
                xtype: 'agreementgrid',
                ref: 'agreementGrid'
            }
        ];
        
        AppContainerUi.superclass.initComponent.call(this);
    }
});